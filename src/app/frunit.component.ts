import { Component } from '@angular/core';


@Component({
  selector: 'app-fruit',
  templateUrl: './fruit.component.html',
  styleUrls: ['./fruit.component.css']
})
export class FruitComponent {
  orange = 0;
  apple = 0;
  grapes = 0;
  bucket: any = [];

  orangeIncrement() {
    if (this.orange !== 10) {
      this.orange++;
      const orange = { name: 'orange', countOrange: this.orange }
      this.bucket.push(orange);
    }
  }
  orangeDecrement() {
    if (this.orange > 0) {
      for (var i = 0; i < this.bucket.length; i++) {
        if (this.bucket[i].name == 'orange') {
          this.bucket.splice(i, 1);
          break;
        }
        this.orange--
      }
    }
  }
  appleIncrement() {
    if (this.apple !== 10) {
      this.apple++;
      const apple = { name: 'apple', countApple: this.apple }
      this.bucket.push(apple);

    }
  }
  appleDecrement() {
    if (this.apple > 0) {
      for (var i = 0; i < this.bucket.length; i++) {
        if (this.bucket[i].name == 'apple') {
          this.bucket.splice(i, 1);
          break;
        }
      }
      this.apple--;
    }

  }
  grapesIncrement() {
    if (this.grapes !== 10) {
      this.grapes++;
      const grapes = { name: 'grapes', countGrapes: this.grapes }
      this.bucket.push(grapes);

    }
  }
  grapesDecrement() {
    if (this.grapes > 0) {
      for (var i = 0; i < this.bucket.length; i++) {
        if (this.bucket[i].name == 'grapes') {
          this.bucket.splice(i, 1);
          break;
        }
      }
      this.grapes--
    }
  }
}
