import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
    `
      :host {
        display: flex;
        justify-content: center;
        margin: 100px 0px;
      }

      .mat-form-field {
        width: 100%;
        min-width: 300px;
      }

      mat-card-title,
      mat-card-content {
        display: flex;
        justify-content: center;
      }

      .error {
        padding: 16px;
        width: 300px;
        color: white;
        background-color: red;
      }

      .button {
        display: flex;
        justify-content: flex-end;
      }
    `,]
})
export class LoginComponents {
  private authData = {
    "user1": {
      "name": 'Admin',
      "permisson":"all",
      "password": "Admin"
    },
    "user2":{
      "name": 'MyName',
      "permisson":"none",
      "password": "test"
    }
  }
  constructor(private router: Router) {}

  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });
  submit() {
    if (this.form.value.username === 'Admin' && this.form.value.password === 'Admin') {
      this.router.navigate(['/fruit']);
    }
  }
  
}
