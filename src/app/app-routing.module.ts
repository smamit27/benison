import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FruitComponent } from './frunit.component';
import { LoginComponents } from './login.components';

const routes: Routes = [

  { path: '', component: LoginComponents},
   { path: 'fruit',component: FruitComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
